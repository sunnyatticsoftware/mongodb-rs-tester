using MongoDB.Bson;
using MongoDB.Driver;
using Xunit.Abstractions;

namespace MongoDbConnection.FunctionalTests;

public class ConnectorTests
{
	private readonly ITestOutputHelper _testOutputHelper;

	public ConnectorTests(ITestOutputHelper testOutputHelper)
	{
		_testOutputHelper = testOutputHelper;
	}

	[Fact]
	public async Task It_Should_Connect_To_Replica_Set_Database()
	{
		const string connectionString = "mongodb://mongors:27017?replicaSet=rs0";
		_testOutputHelper.WriteLine($"Connecting to {connectionString}...");
		var client = new MongoClient(connectionString);

		var database = client.GetDatabase("admin");
		var command = new BsonDocumentCommand<BsonDocument>(new BsonDocument { { "ping", 1 } });
		_ = await database.RunCommandAsync(command);
		_testOutputHelper.WriteLine("Ping successful");
	}
}